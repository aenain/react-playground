var webpack = require('karma-webpack');
var webpackConfig = require('./config/webpack.config.dev.js');

webpackConfig.entry = {};
webpackConfig.module = Object.assign({}, webpackConfig.module, {
    loaders: webpackConfig.module.loaders.map((loader) => {
        return Object.assign({}, loader, { include: undefined });
    }),
    preLoaders: webpackConfig.module.preLoaders.map((loader) => {
        return Object.assign({}, loader, { include: undefined });
    })
});

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',

    browserNoActivityTimeout: 100 * 1000, // FIXME

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
        'tests.webpack.js'
        // { pattern: 'node_modules/babel-polyfill/dist/polyfill.js', watched: false },
        // { pattern: 'test/integration/**/*test.js', watched: false }
        // each file acts as entry point for the webpack configuration
    ],


    // list of files to exclude
    exclude: [
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        // add webpack as preprocessor
        'tests.webpack.js': ['webpack', 'sourcemap']
    },


    webpack: {
        devtool: 'inline-source-map',
        module: webpackConfig.module
    },

    webpackServer: {
        noInfo: true
    },

    webpackMiddleware: {
        // webpack-dev-middleware configuration 
        // i. e. 
        stats: 'errors-only'
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
